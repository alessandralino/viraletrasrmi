#Vira-Letras com RMI

Objetivo: Reimplementar o Jogo da Tabuleiro do trabalho de Sockets, trocando o
mecanismo de comunicação em Sockets para Java RMI. Toda   e   qualquer   comunicação   deve   ser   implementada   SOMENTE   e EXCLUSIVAMENTE por meio de Java RMI. Não deve haver NENHUM código com Socket no projeto.


O Vira-Letras deve ser jogado com dois participantes, que usam um tabuleiro para
jogar.

• Jogadores – 2

• 2 dados

• Tabuleiro com 64 posições (8x8) com as letras do alfabeto distribuídas
aleatoriamente, sendo vogais: 7 letras A, 7 letras E, 6 letra I, 7 letras O, 4 letras
U. Consoantes: 2 letras B, 2 letras C, 2 letras D, 1 letra F, 1 letra G, 1 letra H, 1
letra J, 3 letras L, 2 letras M, 2 letras N, 2 letras P, 1 letra Qu, 3 letra R, 4 letra
S, 2 letra T, 2 letra V, 1 letra X, não contém letras K, W, Y.

#O Jogo

• Sorteie o primeiro jogador para iniciar a partida.

• Na sua vez de jogar, cada jogador rolará simultaneamente os dois dados. Ele
deverá contar os pontos dos dados e desvirar uma quantidade igual de letras do
tabuleiro.


• O participante deverá procurar formar uma ou mais palavras com as letras que
desvirou. Depois de formar as palavras, ele deverá escrevê-las na sua folha,
virar as letras que não usou e retirar do tabuleiro as letras que usou nas palavras
formadas.

• Se o participante não conseguir formar nenhuma palavras, deverá virar
novamente todas as letras no tabuleiro e passar a vez ao próximo.

• O jogo termina quando todas as letras do tabuleiro houverem se esgotado, ou
quando os participantes concordarem que não é mais possível formar nenhuma
palavra com as letras restantes.

• Ao final do jogo, cada participante deverá contar quantas letras obteve no total,
com as palavras que formou. Vence o jogador que tiver mais letras.

• Podem ser formados todos os tipos de palavras, exceto nomes próprios.

#Funcionalidades Básicas
- Controle de turno, com definição de quem inicia a partida
- Detecção de desistência
- Chat para comunicação durante toda a partida
- Reiniciar partida

#Execução

1. Importar projeto na IDE Eclipse Mars;
2. Executar classe ServidorController;
2. Executar classe ClienteController duas vezes (uma para cada jogador).

#Dados do desenvolvedor

Nome: Alessandra Lino Cavalcante | Matrícula: 201017020329

#IDE

Eclipse Java EE IDE for Web Developers.
Version: Mars.2 Release (4.5.2) Build id: 20160218-0600

#Compilador/Interpretador

java version "1.7.0_101"
OpenJDK Runtime Environment (IcedTea 2.6.6) (7u101-2.6.6-0ubuntu0.14.04.1)
OpenJDK 64-Bit Server VM (build 24.95-b01, mixed mode)


#Sistema Operacional

elementary OS 0.3 Freya (64-bit) - Construído sobre Ubuntu 14.04

#APIS (dist/lib) 
-