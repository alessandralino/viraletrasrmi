package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.Jogador;
import model.Peca;
import model.Tabuleiro;

public class TabuleiroView implements Serializable {
	private static final long serialVersionUID = 1L;

	JFrame jFrameTabuleiro;

	private JPanel painelTabuleiro;
	private JPanel painelLetras;

	private GridLayout layoutLetras;

	private List<String> listaLetrasPecaTabuleiroView;
	private List<JLabel> labelLetras;

	private JLabel labelPalavrasJogador1;
	private JLabel labelPalavrasJogador2;

	private JPanel painelDados;
	private JPanel painelDadosEresultadoSorteioDados;
	private JPanel painelDadoEsorteio;

	private JPanel painelPalavrasJogadores;
	private JPanel painelPalavrasJogador1;
	private JPanel painelPalavrasJogador2;

	private JPanel painelDado;

	private JPanel painelResultadoSorteioDado;

	private JPanel painelStatusTabuleiro;

	private JLabel labelResultadoSorteioDado1;
	private JLabel labelResultadoSorteioDado2;
	private JLabel labelStatusTabuleiro;

	private JButton botaoDado;

	private JButton botaoIniciar;
	private JButton botaoReiniciar;
	private JButton botaoCancelar;
	private JButton botaoFechar;
	private JButton botaoEnviarPalavraFormada;
	private JButton botaoIniciarJogo;
	private JButton botaoTerminarJogo;

	private int sorteioDado;
	private int sorteioDado2;
	private int letrasDesviradas;

	private JScrollPane scrollPanePainelPalavrasJogador1;
	private JScrollPane scrollPanePainelPalavrasJogador2;

	private JTextArea jTAreaMensagensChat;
	private JTextArea jTextAreaPalavrasJogador1;
	private JTextArea jTextAreaPalavrasJogador2;

	private JPanel painelPalavraFormada;
	private JPanel painelPalavra;
	private JPanel painelEnviarPalavraFormada;
	private JPanel painelAcoesUsuario;
	private JPanel painelChatEmensagem;
	private JPanel painelChat;
	private JPanel painelMensagem;

	private JTextField jTextFieldMensagemChat;
	private JTextField jTextFieldPalavraFormada;

	private Tabuleiro tabuleiro;

	public TabuleiroView(Tabuleiro tabuleiro) {
		super();

		jFrameTabuleiro = new JFrame();
		this.tabuleiro = tabuleiro;

		preparaPainelStatusTabuleiro();
		preparaPainelLetras();
		preparaPainelDadosEResultados();

		prepraPainelPalavrasFormadas();
		preparaPainelPalavrasJogadores();
		preparaPainelAcoesUsuario();

		inicializarJogo();
		inicializaFrame();
		inicializaChat();

		jFrameTabuleiro.add(painelTabuleiro);
		jFrameTabuleiro.add(painelChatEmensagem);

	}

	public void inicializaFrame() {
		jFrameTabuleiro = new JFrame("Vira-Letras");
		GridLayout layoutTabuleiro = new GridLayout(1, 2);
		jFrameTabuleiro.setLayout(layoutTabuleiro);
		jFrameTabuleiro.setSize(900, 740);
		jFrameTabuleiro.setResizable(false);
		jFrameTabuleiro.setLocationRelativeTo(null);
		jFrameTabuleiro.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrameTabuleiro.setVisible(true);

	}

	public void inicializaChat() {
		painelChatEmensagem = new JPanel();
		painelChat = new JPanel();
		painelMensagem = new JPanel();
		jTAreaMensagensChat = new JTextArea();
		jTextFieldMensagemChat = new JTextField();
		botaoIniciarJogo = new JButton("Iniciar Jogo");

		GridLayout layoutPainelMensagem = new GridLayout(1, 2);
		painelMensagem.setLayout(layoutPainelMensagem);

		painelChatEmensagem.setPreferredSize(new Dimension(440, 280));
		painelChat.setPreferredSize(new Dimension(400, 600));
		painelMensagem.setPreferredSize(new Dimension(400, 40));

		jTextFieldMensagemChat.setPreferredSize(new Dimension(400, 25));
		jTAreaMensagensChat.setPreferredSize(new Dimension(380, 580));
		jTAreaMensagensChat.setEditable(false);

		painelChatEmensagem.setBackground(new Color(111, 151, 200));

		painelChat.setBackground(new Color(249, 251, 253));
		painelMensagem.setBackground(new Color(111, 151, 200));
		botaoIniciarJogo.setPreferredSize(new Dimension(40, 25));

		painelChat.add(jTAreaMensagensChat);

		painelMensagem.add(jTextFieldMensagemChat);
		painelMensagem.add(botaoIniciarJogo);

		painelChatEmensagem.add(painelChat);
		painelChatEmensagem.add(painelMensagem);

	}

	public void inicializarJogo() {
		painelTabuleiro = new JPanel();
		painelTabuleiro.setPreferredSize(new Dimension(300, 500));
		painelTabuleiro.add(painelStatusTabuleiro);
		painelTabuleiro.add(painelLetras);
		painelTabuleiro.add(painelDados);
		painelTabuleiro.add(painelPalavraFormada);
		painelTabuleiro.add(painelPalavrasJogadores);
		painelTabuleiro.add(painelAcoesUsuario);

	}

	public void preparaPainelStatusTabuleiro() {
		painelStatusTabuleiro = new JPanel();
		painelStatusTabuleiro.setPreferredSize(new Dimension(440, 50));

		labelStatusTabuleiro = new JLabel("Status do Jogo: Aguarde a jogada do seu adversário! :)");
		labelStatusTabuleiro.setPreferredSize(new Dimension(440, 30));

		painelStatusTabuleiro.add(labelStatusTabuleiro, BorderLayout.CENTER);
	}

	public void preparaPainelAcoesUsuario() {
		painelAcoesUsuario = new JPanel();
		painelAcoesUsuario.setPreferredSize((new Dimension(445, 30)));

		botaoReiniciar = new JButton("Reiniciar");
		botaoReiniciar.setPreferredSize(new Dimension(140, 20));

		botaoCancelar = new JButton("Desistir");
		botaoCancelar.setPreferredSize(new Dimension(140, 20));

		botaoTerminarJogo = new JButton("Terminar Jogo");
		botaoTerminarJogo.setPreferredSize(new Dimension(140, 20));

		painelAcoesUsuario.add(botaoTerminarJogo);
		painelAcoesUsuario.add(botaoReiniciar);
		painelAcoesUsuario.add(botaoCancelar);

	}

	public void preparaPainelPalavrasJogadores() {
		painelPalavrasJogadores = new JPanel();

		painelPalavrasJogador1 = new JPanel();
		painelPalavrasJogador2 = new JPanel();

		labelPalavrasJogador1 = new JLabel();
		labelPalavrasJogador2 = new JLabel();

		jTextAreaPalavrasJogador1 = new JTextArea();
		jTextAreaPalavrasJogador2 = new JTextArea();

		jTextAreaPalavrasJogador1.setBackground(jFrameTabuleiro.getBackground());
		jTextAreaPalavrasJogador2.setBackground(jFrameTabuleiro.getBackground());

		jTextAreaPalavrasJogador1.setEditable(false);
		jTextAreaPalavrasJogador2.setEditable(false);

		jTextAreaPalavrasJogador1.setPreferredSize(new Dimension(215, 150));
		jTextAreaPalavrasJogador2.setPreferredSize(new Dimension(215, 150));

		painelPalavrasJogador1.setPreferredSize(new Dimension(215, 200));
		painelPalavrasJogador2.setPreferredSize(new Dimension(215, 200));

		labelPalavrasJogador1.setText("Palavras jogador 1");
		labelPalavrasJogador2.setText("Palavras jogador 2");

		painelPalavrasJogador1.add(labelPalavrasJogador1);
		painelPalavrasJogador2.add(labelPalavrasJogador2);

		painelPalavrasJogador1.add(jTextAreaPalavrasJogador1);
		painelPalavrasJogador2.add(jTextAreaPalavrasJogador2);

		scrollPanePainelPalavrasJogador1 = new JScrollPane(painelPalavrasJogador1);
		scrollPanePainelPalavrasJogador1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPanePainelPalavrasJogador1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		scrollPanePainelPalavrasJogador2 = new JScrollPane(painelPalavrasJogador2);
		scrollPanePainelPalavrasJogador2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPanePainelPalavrasJogador2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		painelPalavrasJogadores.add(painelPalavrasJogador1);
		painelPalavrasJogadores.add(painelPalavrasJogador2);

		painelPalavrasJogadores.add(new JScrollPane(painelPalavrasJogador1), BorderLayout.CENTER);
		painelPalavrasJogadores.add(new JScrollPane(painelPalavrasJogador2), BorderLayout.CENTER);

	}

	public void prepraPainelPalavrasFormadas() {
		painelPalavraFormada = new JPanel();
		painelPalavra = new JPanel();
		painelEnviarPalavraFormada = new JPanel();

		botaoEnviarPalavraFormada = new JButton();
		botaoEnviarPalavraFormada.setText("Enviar");

		painelPalavraFormada = new JPanel();
		painelPalavra = new JPanel();
		painelEnviarPalavraFormada = new JPanel();

		jTextFieldPalavraFormada = new JTextField();
		jTextFieldPalavraFormada.setEditable(false);

		painelPalavraFormada.setPreferredSize(new Dimension(440, 50));
		painelPalavra.setPreferredSize(new Dimension(300, 40));
		painelEnviarPalavraFormada.setPreferredSize(new Dimension(100, 40));

		jTextFieldPalavraFormada.setPreferredSize(new Dimension(300, 25));

		painelPalavraFormada.setBackground(new Color(111, 151, 200));
		painelPalavra.setBackground(new Color(111, 151, 200));
		painelEnviarPalavraFormada.setBackground(new Color(111, 151, 200));
		painelEnviarPalavraFormada.add(botaoEnviarPalavraFormada);

		painelPalavra.add(jTextFieldPalavraFormada);

		painelPalavraFormada.add(painelPalavra);
		painelPalavraFormada.add(painelEnviarPalavraFormada);
	}

	public void preparaPainelDadosEResultados() {
		painelDadosEresultadoSorteioDados = new JPanel();
		painelDados = new JPanel();

		painelDado = new JPanel();

		painelDadoEsorteio = new JPanel();

		painelResultadoSorteioDado = new JPanel();

		botaoDado = new JButton();
		botaoDado.setEnabled(false);

		labelResultadoSorteioDado1 = new JLabel();

		Image img = null;
		try {
			img = ImageIO.read(getClass().getResource("/resources/ic_joga_dado.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		botaoDado.setIcon(new ImageIcon(img));
		botaoDado.setBorder(null);
		//botaoDado.setEnabled(false);

		painelDado.setPreferredSize(new Dimension(100, 100));
		painelDado.add(botaoDado);

		painelResultadoSorteioDado.setPreferredSize(new Dimension(100, 97));

		labelResultadoSorteioDado1.setFont(new Font("Consolas", Font.BOLD, 72));
		labelResultadoSorteioDado1.setForeground(new Color(111, 151, 200));

		painelResultadoSorteioDado.add(labelResultadoSorteioDado1);

		painelDadoEsorteio.add(painelDado, new GridBagConstraints());
		painelDadoEsorteio.add(painelResultadoSorteioDado);

		painelResultadoSorteioDado.add(labelResultadoSorteioDado1);

		painelDadosEresultadoSorteioDados.add(painelDadoEsorteio);

		painelDados.add(painelDadosEresultadoSorteioDados);
	}

	public void preparaPainelLetras() {
		painelLetras = new JPanel();
		labelLetras = new ArrayList<JLabel>();
		layoutLetras = new GridLayout(8, 8);

		painelLetras.setPreferredSize(new Dimension(440, 200));
		painelLetras.setLayout(layoutLetras);
		preenchePainelLetras();
	}

	public void preenchePainelLetras() {
		try {

			Image img = ImageIO.read(getClass().getResource("/resources/ic_peca.png"));
			Icon icon = new ImageIcon(img);

			List<Peca> listaDePecas = geraListaLetrasPecaTabuleiro();
			Collections.shuffle(listaDePecas);
			tabuleiro.setListaDePecas(listaDePecas);

			for (int i = 0; i < 64; i++) {
				labelLetras.add(new JLabel(tabuleiro.getListaDePecas().get(i).getConteudo()));

			}

			for (int i = 0; i < 64; i++) {
				labelLetras.get(i).setIcon(icon);
				painelLetras.add(labelLetras.get(i), BorderLayout.CENTER);

			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	public int jogaDados() {
		Random rand1 = new Random();
		int sorteio = rand1.nextInt(6) + 1;
		Random rand2 = new Random();
		sorteio += rand2.nextInt(6) + 1;
		return sorteio;
	}

	public List<Peca> geraListaLetrasPecaTabuleiro() {
		int i;
		List<Peca> listaDePecas = new ArrayList<Peca>();

		// 7 ---- A, E, O, 21
		for (i = 0; i < 7; i++) {
			listaDePecas.add(new Peca("A", true));
			listaDePecas.add(new Peca("E", true));
			listaDePecas.add(new Peca("O", true));

		}
		// 6 ---- I, 6
		for (i = 0; i < 6; i++) {
			listaDePecas.add(new Peca("I", true));
		}

		// 4 ---- U, S, 8
		for (i = 0; i < 4; i++) {
			listaDePecas.add(new Peca("U", true));
			listaDePecas.add(new Peca("S", true));

		}

		// 3 ---- L, R, 6
		for (i = 0; i < 3; i++) {
			listaDePecas.add(new Peca("L", true));
			listaDePecas.add(new Peca("R", true));
		}

		// 2 ---- B, C, D, M, N, P,T, V 16
		for (i = 0; i < 2; i++) {
			listaDePecas.add(new Peca("B", true));
			listaDePecas.add(new Peca("C", true));
			listaDePecas.add(new Peca("D", true));
			listaDePecas.add(new Peca("M", true));
			listaDePecas.add(new Peca("N", true));
			listaDePecas.add(new Peca("P", true));
			listaDePecas.add(new Peca("T", true));
			listaDePecas.add(new Peca("V", true));
		}
		// 1 ---- F, G, H, J, Qu, X

		listaDePecas.add(new Peca("F", true));
		listaDePecas.add(new Peca("L", true));
		listaDePecas.add(new Peca("G", true));
		listaDePecas.add(new Peca("H", true));
		listaDePecas.add(new Peca("J", true));
		listaDePecas.add(new Peca("Qu", true));
		listaDePecas.add(new Peca("X", true));

		return listaDePecas;
	}

	public static void main(String[] args) {
		Tabuleiro tabuleiro = new Tabuleiro();
		new TabuleiroView(tabuleiro);
	}

	public JPanel getPainelTabuleiro() {
		return painelTabuleiro;
	}

	public void setPainelTabuleiro(JPanel painelTabuleiro) {
		this.painelTabuleiro = painelTabuleiro;
	}

	public JPanel getPainelLetras() {
		return painelLetras;
	}

	public void setPainelLetras(JPanel painelLetras) {
		this.painelLetras = painelLetras;
	}

	public GridLayout getLayoutLetras() {
		return layoutLetras;
	}

	public void setLayoutLetras(GridLayout layoutLetras) {
		this.layoutLetras = layoutLetras;
	}

	public List<String> getListaLetrasPecaTabuleiroView() {
		return listaLetrasPecaTabuleiroView;
	}

	public void setListaLetrasPecaTabuleiroView(List<String> listaLetrasPecaTabuleiroView) {
		this.listaLetrasPecaTabuleiroView = listaLetrasPecaTabuleiroView;
	}

	public List<JLabel> getLabelLetras() {
		return labelLetras;
	}

	public void setLabelLetras(List<JLabel> labelLetras) {
		this.labelLetras = labelLetras;
	}

	public JLabel getLabelPalavrasJogador1() {
		return labelPalavrasJogador1;
	}

	public void setLabelPalavrasJogador1(JLabel labelPalavrasJogador1) {
		this.labelPalavrasJogador1 = labelPalavrasJogador1;
	}

	public JLabel getLabelPalavrasJogador2() {
		return labelPalavrasJogador2;
	}

	public void setLabelPalavrasJogador2(JLabel labelPalavrasJogador2) {
		this.labelPalavrasJogador2 = labelPalavrasJogador2;
	}

	public JPanel getPainelDados() {
		return painelDados;
	}

	public void setPainelDados(JPanel painelDados) {
		this.painelDados = painelDados;
	}

	public JPanel getPainelDadosEresultadoSorteioDados() {
		return painelDadosEresultadoSorteioDados;
	}

	public void setPainelDadosEresultadoSorteioDados(JPanel painelDadosEresultadoSorteioDados) {
		this.painelDadosEresultadoSorteioDados = painelDadosEresultadoSorteioDados;
	}

	public JPanel getPainelDadoEsorteio1() {
		return painelDadoEsorteio;
	}

	public void setPainelDadoEsorteio1(JPanel painelDadoEsorteio1) {
		this.painelDadoEsorteio = painelDadoEsorteio1;
	}

	public JPanel getPainelPalavrasJogadores() {
		return painelPalavrasJogadores;
	}

	public void setPainelPalavrasJogadores(JPanel painelPalavrasJogadores) {
		this.painelPalavrasJogadores = painelPalavrasJogadores;
	}

	public JPanel getPainelPalavrasJogador1() {
		return painelPalavrasJogador1;
	}

	public void setPainelPalavrasJogador1(JPanel painelPalavrasJogador1) {
		this.painelPalavrasJogador1 = painelPalavrasJogador1;
	}

	public JPanel getPainelDado1() {
		return painelDado;
	}

	public void setPainelDado1(JPanel painelDado1) {
		this.painelDado = painelDado1;
	}

	public JPanel getPainelResultadoSorteioDado1() {
		return painelResultadoSorteioDado;
	}

	public void setPainelResultadoSorteioDado1(JPanel painelResultadoSorteioDado1) {
		this.painelResultadoSorteioDado = painelResultadoSorteioDado1;
	}

	public JLabel getLabelResultadoSorteioDado1() {
		return labelResultadoSorteioDado1;
	}

	public void setLabelResultadoSorteioDado1(JLabel labelResultadoSorteioDado1) {
		this.labelResultadoSorteioDado1 = labelResultadoSorteioDado1;
	}

	public JLabel getLabelResultadoSorteioDado2() {
		return labelResultadoSorteioDado2;
	}

	public void setLabelResultadoSorteioDado2(JLabel labelResultadoSorteioDado2) {
		this.labelResultadoSorteioDado2 = labelResultadoSorteioDado2;
	}

	public JButton getBotaoDado1() {
		return botaoDado;
	}

	public void setBotaoDado1(JButton botaoDado1) {
		this.botaoDado = botaoDado1;
	}

	public JButton getBotaoIniciar() {
		return botaoIniciar;
	}

	public void setBotaoIniciar(JButton botaoIniciar) {
		this.botaoIniciar = botaoIniciar;
	}

	public JButton getBotaoReiniciar() {
		return botaoReiniciar;
	}

	public void setBotaoReiniciar(JButton botaoReiniciar) {
		this.botaoReiniciar = botaoReiniciar;
	}

	public JButton getBotaoCancelar() {
		return botaoCancelar;
	}

	public void setBotaoCancelar(JButton botaoCancelar) {
		this.botaoCancelar = botaoCancelar;
	}

	public JButton getBotaoFechar() {
		return botaoFechar;
	}

	public void setBotaoFechar(JButton botaoFechar) {
		this.botaoFechar = botaoFechar;
	}

	public JButton getBotaoEnviarPalavraFormada() {
		return botaoEnviarPalavraFormada;
	}

	public void setBotaoEnviarMensagem(JButton botaoEnviarMensagem) {
		this.botaoEnviarPalavraFormada = botaoEnviarMensagem;
	}

	public int getSorteioDado1() {
		return sorteioDado;
	}

	public void setSorteioDado1(int sorteioDado1) {
		this.sorteioDado = sorteioDado1;
	}

	public int getSorteioDado2() {
		return sorteioDado2;
	}

	public void setSorteioDado2(int sorteioDado2) {
		this.sorteioDado2 = sorteioDado2;
	}

	public int getLetrasDesviradas() {
		return letrasDesviradas;
	}

	public void setLetrasDesviradas(int letrasDesviradas) {
		this.letrasDesviradas = letrasDesviradas;
	}

	public JPanel getPainelPalavraFormada() {
		return painelPalavraFormada;
	}

	public void setPainelPalavraFormada(JPanel painelPalavraFormada) {
		this.painelPalavraFormada = painelPalavraFormada;
	}

	public JPanel getPainelPalavra() {
		return painelPalavra;
	}

	public void setPainelPalavra(JPanel painelPalavra) {
		this.painelPalavra = painelPalavra;
	}

	public JPanel getPainelEnviarPalavra() {
		return painelEnviarPalavraFormada;
	}

	public void setPainelEnviarPalavra(JPanel painelEnviarPalavra) {
		this.painelEnviarPalavraFormada = painelEnviarPalavra;
	}

	public JPanel getPainelAcoesUsuario() {
		return painelAcoesUsuario;
	}

	public void setPainelAcoesUsuario(JPanel painelAcoesUsuario) {
		this.painelAcoesUsuario = painelAcoesUsuario;
	}

	public JTextField getjTextFieldPalavraFormada() {
		return jTextFieldPalavraFormada;
	}

	public void setjTextFieldPalavraFormada(JTextField jTextFieldPalavraFormada) {
		this.jTextFieldPalavraFormada = jTextFieldPalavraFormada;
	}

	public JLabel getLabelStatusTabuleiro() {
		return labelStatusTabuleiro;
	}

	public void setLabelStatusTabuleiro(JLabel labelStatusTabuleiro) {
		this.labelStatusTabuleiro = labelStatusTabuleiro;
	}

	public JFrame getjFrameTabuleiro() {
		return jFrameTabuleiro;
	}

	public void setjFrameTabuleiro(JFrame jFrameTabuleiro) {
		this.jFrameTabuleiro = jFrameTabuleiro;
	}

	public JButton getBotaoIniciarJogo() {
		return botaoIniciarJogo;
	}

	public void setBotaoIniciarJogo(JButton botaoIniciarJogo) {
		this.botaoIniciarJogo = botaoIniciarJogo;
	}

	public JPanel getPainelStatusTabuleiro() {
		return painelStatusTabuleiro;
	}

	public void setPainelStatusTabuleiro(JPanel painelStatusTabuleiro) {
		this.painelStatusTabuleiro = painelStatusTabuleiro;
	}

	public JScrollPane getScrollPanePainelPalavrasJogador1() {
		return scrollPanePainelPalavrasJogador1;
	}

	public void setScrollPanePainelPalavrasJogador1(JScrollPane scrollPanePainelPalavrasJogador1) {
		this.scrollPanePainelPalavrasJogador1 = scrollPanePainelPalavrasJogador1;
	}

	public JScrollPane getScrollPanePainelPalavrasJogador2() {
		return scrollPanePainelPalavrasJogador2;
	}

	public void setScrollPanePainelPalavrasJogador2(JScrollPane scrollPanePainelPalavrasJogador2) {
		this.scrollPanePainelPalavrasJogador2 = scrollPanePainelPalavrasJogador2;
	}

	public JTextArea getjTAreaMensagensChat() {
		return jTAreaMensagensChat;
	}

	public void setjTAreaMensagensChat(JTextArea jTAreaMensagensChat) {
		this.jTAreaMensagensChat = jTAreaMensagensChat;
	}

	public JPanel getPainelChatEmensagem() {
		return painelChatEmensagem;
	}

	public void setPainelChatEmensagem(JPanel painelChatEmensagem) {
		this.painelChatEmensagem = painelChatEmensagem;
	}

	public JPanel getPainelChat() {
		return painelChat;
	}

	public void setPainelChat(JPanel painelChat) {
		this.painelChat = painelChat;
	}

	public JPanel getPainelMensagem() {
		return painelMensagem;
	}

	public void setPainelMensagem(JPanel painelMensagem) {
		this.painelMensagem = painelMensagem;
	}

	public JTextField getjTextFieldMensagemChat() {
		return jTextFieldMensagemChat;
	}

	public void setjTextFieldMensagemChat(JTextField jTextFieldMensagemChat) {
		this.jTextFieldMensagemChat = jTextFieldMensagemChat;
	}

	public JPanel getPainelDadoEsorteio() {
		return painelDadoEsorteio;
	}

	public void setPainelDadoEsorteio(JPanel painelDadoEsorteio) {
		this.painelDadoEsorteio = painelDadoEsorteio;
	}

	public JPanel getPainelPalavrasJogador2() {
		return painelPalavrasJogador2;
	}

	public void setPainelPalavrasJogador2(JPanel painelPalavrasJogador2) {
		this.painelPalavrasJogador2 = painelPalavrasJogador2;
	}

	public JPanel getPainelDado() {
		return painelDado;
	}

	public void setPainelDado(JPanel painelDado) {
		this.painelDado = painelDado;
	}

	public JPanel getPainelResultadoSorteioDado() {
		return painelResultadoSorteioDado;
	}

	public void setPainelResultadoSorteioDado(JPanel painelResultadoSorteioDado) {
		this.painelResultadoSorteioDado = painelResultadoSorteioDado;
	}

	public JButton getBotaoDado() {
		return botaoDado;
	}

	public void setBotaoDado(JButton botaoDado) {
		this.botaoDado = botaoDado;
	}

	public int getSorteioDado() {
		return sorteioDado;
	}

	public void setSorteioDado(int sorteioDado) {
		this.sorteioDado = sorteioDado;
	}

	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}

	public void setTabuleiro(Tabuleiro tabuleiro) {
		this.tabuleiro = tabuleiro;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JTextArea getjTextAreaPalavrasJogador1() {
		return jTextAreaPalavrasJogador1;
	}

	public void setjTextAreaPalavrasJogador1(JTextArea jTextAreaPalavrasJogador1) {
		this.jTextAreaPalavrasJogador1 = jTextAreaPalavrasJogador1;
	}

	public JTextArea getjTextAreaPalavrasJogador2() {
		return jTextAreaPalavrasJogador2;
	}

	public void setjTextAreaPalavrasJogador2(JTextArea jTextAreaPalavrasJogador2) {
		this.jTextAreaPalavrasJogador2 = jTextAreaPalavrasJogador2;
	}

	public JPanel getPainelEnviarPalavraFormada() {
		return painelEnviarPalavraFormada;
	}

	public void setPainelEnviarPalavraFormada(JPanel painelEnviarPalavraFormada) {
		this.painelEnviarPalavraFormada = painelEnviarPalavraFormada;
	}

	public void setBotaoEnviarPalavraFormada(JButton botaoEnviarPalavraFormada) {
		this.botaoEnviarPalavraFormada = botaoEnviarPalavraFormada;
	}

	public JButton getBotaoTerminarJogo() {
		return botaoTerminarJogo;
	}

	public void setBotaoTerminarJogo(JButton botaoTerminarJogo) {
		this.botaoTerminarJogo = botaoTerminarJogo;
	}

}
