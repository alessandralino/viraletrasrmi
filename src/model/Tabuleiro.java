package model;

import java.io.Serializable;
import java.util.List;

public class Tabuleiro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isCancelado;
	private boolean isTerminado;
	private int dado;
	private List<Peca> listaDePecas;
	private List<Integer> listaIndiceLetrasUsadasNaPalavraFormada;
	private String palavraFormada;

	public Tabuleiro() {
		super();

	}

	public int getDado() {
		return dado;
	}

	public void setDado(int dado) {
		this.dado = dado;
	}

	public List<Peca> getListaDePecas() {
		return listaDePecas;
	}

	public void setListaDePecas(List<Peca> listaDePecas) {
		this.listaDePecas = listaDePecas;
	}

	public String getPalavraFormada() {
		return palavraFormada;
	}

	public void setPalavraFormada(String palavraFormada) {
		this.palavraFormada = palavraFormada;
	}

	public boolean isCancelado() {
		return isCancelado;
	}

	public void setCancelado(boolean isCancelado) {
		this.isCancelado = isCancelado;
	}

	public List<Integer> getListaIndiceLetrasUsadasNaPalavraFormada() {
		return listaIndiceLetrasUsadasNaPalavraFormada;
	}

	public void setListaIndiceLetrasUsadasNaPalavraFormada(List<Integer> listaIndiceLetrasUsadasNaPalavraFormada) {
		this.listaIndiceLetrasUsadasNaPalavraFormada = listaIndiceLetrasUsadasNaPalavraFormada;
	}

	public boolean isTerminado() {
		return isTerminado;
	}

	public void setTerminado(boolean isTerminado) {
		this.isTerminado = isTerminado;
	}
	
	

}
