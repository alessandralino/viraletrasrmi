package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Jogada implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private boolean isJogadaValida;
	private String palavraFormada;
	private List<Peca> listaLetrasDesviradas;
	private List<Peca> listaLetrasUsadasNaPalavraFormada;
	private List<Peca> listaPecasCompoemPalaraFormada;

	public Jogada() {
		super();
		this.listaLetrasDesviradas = new ArrayList<Peca>();
		this.listaLetrasUsadasNaPalavraFormada = new ArrayList<Peca>();
		this.listaPecasCompoemPalaraFormada = new ArrayList<Peca>();
		this.isJogadaValida = true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPalavraFormada() {
		return palavraFormada;
	}

	public void setPalavraFormada(String palavraFormada) {
		this.palavraFormada = palavraFormada;
	}

	public List<Peca> getListaPalavrasDesviradas() {
		return listaLetrasDesviradas;
	}

	public void setListaPalavrasDesviradas(List<Peca> listaPalavrasDesviradas) {
		this.listaLetrasDesviradas = listaPalavrasDesviradas;
	}

	public boolean isJogadaValida() {
		return isJogadaValida;
	}

	public void setJogadaValida(boolean isJogadaValida) {
		this.isJogadaValida = isJogadaValida;
	}

	public List<Peca> getListaLetrasDesviradas() {
		return listaLetrasDesviradas;
	}

	public void setListaLetrasDesviradas(List<Peca> listaLetrasDesviradas) {
		this.listaLetrasDesviradas = listaLetrasDesviradas;
	}

	public List<Peca> getListaLetrasUsadasNaPalavraFormada() {
		return listaLetrasUsadasNaPalavraFormada;
	}

	public void setListaLetrasUsadasNaPalavraFormada(List<Peca> listaLetrasUsadasNaPalavraFormada) {
		this.listaLetrasUsadasNaPalavraFormada = listaLetrasUsadasNaPalavraFormada;
	}

	public List<Peca> getListaPecasCompoemPalavraFormada() {
		return listaPecasCompoemPalaraFormada;
	}

	public void setListaPecasCompoemPalaraFormada(List<Peca> listaPecasCompoemPalaraFormada) {
		this.listaPecasCompoemPalaraFormada = listaPecasCompoemPalaraFormada;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
