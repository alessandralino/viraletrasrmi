package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Jogador implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String nome;
	private Tabuleiro tabuleiro;
	private boolean isJogando;
	private boolean isVez;
	private boolean isDesistente;
	private boolean isVencedor;
	private boolean isJogador1;
	private Jogada jogada;
	private List<String> listaDePalavrasFormadas;

	public Jogador() {
		super();
		this.isJogando = true;
		this.isVez = false;
		this.isJogador1 = false;
		this.isVencedor = false;
		this.isDesistente = false;
		this.jogada = new Jogada();
		this.tabuleiro = new Tabuleiro();
		this.listaDePalavrasFormadas = new ArrayList<String>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}

	public void setTabuleiro(Tabuleiro tabuleiro) {
		this.tabuleiro = tabuleiro;
	}

	public boolean isJogando() {
		return isJogando;
	}

	public void setJogando(boolean isJogando) {
		this.isJogando = isJogando;
	}

	public Jogada getJogada() {
		return jogada;
	}

	public void setJogada(Jogada jogada) {
		this.jogada = jogada;
	}

	public List<String> getListaDePalavrasFormadas() {
		return listaDePalavrasFormadas;
	}

	public void setListaDePalavrasFormadas(List<String> listaDePalavrasFormadas) {
		this.listaDePalavrasFormadas = listaDePalavrasFormadas;
	}

	public boolean isJogador1() {
		return isJogador1;
	}

	public void setJogador1(boolean isJogador1) {
		this.isJogador1 = isJogador1;
	}

	public boolean isVencedor() {
		return isVencedor;
	}

	public void setVencedor(boolean isVencedor) {
		this.isVencedor = isVencedor;
	}
	

	public boolean isDesistente() {
		return isDesistente;
	}

	public void setDesistente(boolean isDesistente) {
		this.isDesistente = isDesistente;
	}
	
	public boolean isVez() {
		return isVez;
	}

	public void setVez(boolean isVez) {
		this.isVez = isVez;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
