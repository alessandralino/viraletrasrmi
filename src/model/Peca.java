package model;

import java.io.Serializable;

public class Peca implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String conteudo;
	private boolean isTabuleiro;
	private boolean isUsada;

	public Peca(String conteudo, boolean isUsada) {
		super();
		this.conteudo = conteudo;
		this.isUsada = isUsada;

	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public boolean isTabuleiro() {
		return isTabuleiro;
	}

	public void setTabuleiro(boolean isTabuleiro) {
		this.isTabuleiro = isTabuleiro;
	}

	public boolean isUsada() {
		return isUsada;
	}

	public void setUsada(boolean isUsada) {
		this.isUsada = isUsada;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
