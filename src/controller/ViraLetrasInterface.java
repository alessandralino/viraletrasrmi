package controller;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import model.Jogador;
import model.Peca;
import model.Tabuleiro;

public interface ViraLetrasInterface extends Remote {
	public int conectarCliente(String nome, Tabuleiro tabuleiro) throws RemoteException;

	public void enviarMensagemChat(int id, String mensagem) throws RemoteException;

	public String receberMensagemChat() throws RemoteException;

	public boolean checkMensagem(int id) throws RemoteException;

	public Tabuleiro receberTabuleiro() throws RemoteException;
	
	public Tabuleiro receberTabuleiroSemLetrasUsadas() throws RemoteException;

	public boolean checkTabuleiro(int id) throws RemoteException;

	public void enviarTabuleiro(int id, Tabuleiro tabuleiro) throws RemoteException;

	public void enviarPosicaoTabuleiroDesvirada(int posicao) throws RemoteException;
	
	public void enviarPosicaoTabuleiroUsada(int posicao) throws RemoteException;
	
	public List<Integer> receberListaPosicaoTabuleiroUsada() throws RemoteException;

	public void enviarSorteioDado(int sorteio) throws RemoteException;

	public int receberSorteioDado() throws RemoteException;

	public boolean checkSorteioDado(int id) throws RemoteException;

	public boolean checkJogada(int id) throws RemoteException;
	
	public boolean checkJogadaOutroJogador(int id) throws RemoteException;

	public Tabuleiro receberJogada() throws RemoteException;

	public boolean existemAdversarios() throws RemoteException;

	public void enviarPalavraFormada(int id, String palavraFormada) throws RemoteException;

	public String receberPalavraFormada() throws RemoteException;

	public boolean checkPalavraAdversario(int id, boolean avaliacao) throws RemoteException;

	public boolean checkPalavraFormada(int id) throws RemoteException;

	public String nomeJogador(int id) throws RemoteException;

	public void salvarPalavraFormada(int id, String palavraFormada) throws RemoteException;

	public boolean checkPlayer1(int id) throws RemoteException;

	public List<String> listaPalavrasFormadasJogador(int id) throws RemoteException;

	public boolean checkListaPalavrasFormadasJogador(int id) throws RemoteException;
	
	public Tabuleiro tabuleiroReiniciado() throws RemoteException;

	public void isListar(boolean listar) throws RemoteException;
	
	public void criarNovoTabuleiroRenicio(int id) throws RemoteException;
	
	public boolean autorizarReinicio(int id, boolean avaliacao) throws RemoteException;

	public Map<Integer, Jogador> getJogadores() throws RemoteException;

	public void jogadorFechouAJanela(int id, boolean janelaStatus) throws RemoteException;

	public boolean checkJanelaAberta(int id) throws RemoteException;

	public void mudarStatusJanela() throws RemoteException;

	public void informarDesistencia(int id) throws RemoteException;
	
	public void informarPassarAvez(int id) throws RemoteException;
	
	public void informarReiniciarJogo(int id) throws RemoteException;

	public void mudarStatusDesistencia(boolean avaliacao) throws RemoteException;

	public boolean checkDesistencia(int id) throws RemoteException;
	
	public boolean checkReiniciarJogo(int id) throws RemoteException;

	public boolean checkTabuleiroCancelado(int id) throws RemoteException;

	public boolean autorizarDesistencia(int id, boolean avaliacao) throws RemoteException;

	public void criarNovoTabuleiroDesistente(int id) throws RemoteException;//criarNovoTabuleiroTermino
	
	public void criarNovoTabuleiroTermino(int id) throws RemoteException;//

	public boolean checkTabuleiroJogoTerminado() throws RemoteException;

	public Tabuleiro tabuleiroCancelado() throws RemoteException;


	public List<Peca> getListaDePosicoesUsadasNaPalavra(int id) throws RemoteException;

	public boolean checkTerminarJogo(int id) throws RemoteException;

	public boolean autorizarTermino(int id, boolean avaliacao) throws RemoteException;

	public List<Integer> getListaPosicoesDesviradas() throws RemoteException;

	public void informarTerminarJogo(int id) throws RemoteException;

}
