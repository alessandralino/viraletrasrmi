package controller;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServidorController {
	ViraLetras servidor;
	Registry registro;

	public ServidorController() throws RemoteException {
		try {
			servidor = new ViraLetras();
			registro = LocateRegistry.createRegistry(12345);
			registro.rebind("ServidorViraLetras", servidor);
			System.out.println("Servidor iniciado :D!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		try {
			ServidorController ServidorViraLetras = new ServidorController();
		} catch (RemoteException ex) {
			Logger.getLogger(ServidorController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
