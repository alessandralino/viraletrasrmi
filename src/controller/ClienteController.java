package controller;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.PlainDocument;

import model.Jogador;
import model.Peca;
import model.Tabuleiro;
import view.TabuleiroView;

public class ClienteController {
	Jogador jogador;
	JTextField nomeJogador;
	TabuleiroView tabuleiroView;
	Tabuleiro tabuleiro;
	static boolean termino = true;

	public ClienteController() throws RemoteException {
		this.nomeJogador = new JTextField();
		this.jogador = new Jogador();

		JLabel lblMessage = new JLabel("Digite seu nome");
		Object[] texts = { lblMessage, nomeJogador };
		// JOptionPane.showMessageDialog(null, texts);

		this.tabuleiro = new Tabuleiro();
		this.tabuleiroView = new TabuleiroView(tabuleiro);

		nomeJogador.setText("CLIENTE");
		jogador.setTabuleiro(tabuleiro);
		jogador.setNome(nomeJogador.getText());

		this.tabuleiroView.getjFrameTabuleiro().setTitle(".: Vira-Letras :. Jogador@: " + nomeJogador.getText());
		this.tabuleiroView.getPainelTabuleiro().setVisible(false);

	}

	public static void main(String args[]) throws RemoteException, MalformedURLException, NotBoundException {
		try {
			final ViraLetrasInterface viraLetrasIterface = (ViraLetrasInterface) Naming
					.lookup("//localhost:12345/ServidorViraLetras");
			System.out.println("Servidor localizado!");

			final ClienteController cliente = new ClienteController();

			cliente.jogador.setId(viraLetrasIterface.conectarCliente(cliente.jogador.getNome(), cliente.tabuleiro));

			cliente.tabuleiroView.getjTextFieldPalavraFormada().addKeyListener(new KeyListener() {

				@Override
				public void keyTyped(KeyEvent e) {

					cliente.tabuleiroView.getjTextFieldPalavraFormada()
							.setText(cliente.tabuleiroView.getjTextFieldPalavraFormada().getText().toUpperCase());

				}

				@Override
				public void keyReleased(KeyEvent e) {

				}

				@Override
				public void keyPressed(KeyEvent e) {

				}
			});
			cliente.tabuleiroView.getjTextFieldMensagemChat().addKeyListener(new KeyListener() {

				@Override
				public void keyTyped(KeyEvent e) {

				}

				@Override
				public void keyReleased(KeyEvent e) {

					if (e.getKeyCode() == KeyEvent.VK_ENTER) {

						if (!cliente.tabuleiroView.getjTextFieldMensagemChat().getText().trim().equals("")) {
							cliente.tabuleiroView.getjTAreaMensagensChat().append("Você diz: "
									+ cliente.tabuleiroView.getjTextFieldMensagemChat().getText() + "\r\n");
							try {
								viraLetrasIterface.enviarMensagemChat(cliente.jogador.getId(),
										cliente.tabuleiroView.getjTextFieldMensagemChat().getText());
							} catch (RemoteException e1) {
								e1.printStackTrace();
							} finally {
								cliente.tabuleiroView.getjTextFieldMensagemChat().setText("");
							}
						}

					}
				}

				@Override
				public void keyPressed(KeyEvent arg0) {

				}
			});

			cliente.tabuleiroView.getBotaoIniciarJogo().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						if (viraLetrasIterface.existemAdversarios()) {
							cliente.tabuleiroView.getPainelTabuleiro().setVisible(true);
							cliente.tabuleiroView.getLabelStatusTabuleiro()
									.setText("Status do Jogo: A primeira jogada é a sua!");
							cliente.tabuleiroView.getBotaoIniciarJogo().setEnabled(false);

							cliente.tabuleiroView.getBotaoDado1().setEnabled(true);
							cliente.jogador.setJogador1(true);

							viraLetrasIterface.enviarTabuleiro(cliente.jogador.getId(), cliente.tabuleiro);
						} else {
							JOptionPane.showMessageDialog(cliente.tabuleiroView.getjFrameTabuleiro(),
									"Não há ninguém conectado ainda! ;) ", "Opa!", JOptionPane.INFORMATION_MESSAGE);

						}
					} catch (RemoteException e1) {
						e1.printStackTrace();
					}

				}
			});

			cliente.tabuleiroView.getBotaoDado1().addActionListener(new ActionListener() {

				public int letras;
				public boolean mostrouMensagem = false;
				public boolean inicializarLetrasDesviradas = false;
				StringBuilder letrasDesviradas = new StringBuilder();
				public List<Integer> posicoesClicadas = new ArrayList<Integer>();
				public List<Integer> posicoesClicadasAux = new ArrayList<Integer>();

				@Override
				public void actionPerformed(ActionEvent e) {
					int sorteio = cliente.tabuleiroView.jogaDados();
					cliente.tabuleiroView.getLabelResultadoSorteioDado1().setText(String.valueOf(sorteio));
					cliente.tabuleiroView.setLetrasDesviradas(sorteio);

					letras = cliente.tabuleiroView.getLetrasDesviradas();

					try {
						viraLetrasIterface.enviarSorteioDado(sorteio);
					} catch (RemoteException e2) {
						e2.printStackTrace();
					}

					for (final JLabel labelTabuleiro : cliente.tabuleiroView.getLabelLetras()) {
						labelTabuleiro.addMouseListener(new MouseListener() {
							private int indice;

							@Override
							public void mouseReleased(MouseEvent e) {

							}

							@Override
							public void mousePressed(MouseEvent e) {

							}

							@Override
							public void mouseExited(MouseEvent e) {

							}

							@Override
							public void mouseEntered(MouseEvent e) {

							}

							@Override
							public void mouseClicked(MouseEvent e) {

								if (letras != 0) {

									int y = labelTabuleiro.getY() / 25;
									int x = labelTabuleiro.getX() / 55;

									indice = 8 * y + x;

									if (!posicoesClicadas.contains(indice)) {
										labelTabuleiro.setIcon(null);

										cliente.jogador.getJogada().getListaPalavrasDesviradas()
												.add(new Peca(labelTabuleiro.getText(), true));
										letras--;
										try {
											viraLetrasIterface.enviarPosicaoTabuleiroDesvirada(indice);
											posicoesClicadas = viraLetrasIterface.getListaPosicoesDesviradas();

										} catch (RemoteException e1) {
											e1.printStackTrace();
										}
									}

								}

								if (letras == 0 && mostrouMensagem == false) {
									JOptionPane.showMessageDialog(cliente.tabuleiroView.getjFrameTabuleiro(),
											"Escolha as letras que formarão a palavra a ser enviada!");

									mostrouMensagem = true;

								}

								int y = 0;
								int x = 0;

								if (letras == 0 && mostrouMensagem == true) {
									y = labelTabuleiro.getY() / 25;
									x = labelTabuleiro.getX() / 55;
									indice = 8 * y + x;
									List<Integer> listaPosicoesUsadas = new ArrayList<Integer>();

									try {
										posicoesClicadas = viraLetrasIterface.getListaPosicoesDesviradas();
									} catch (RemoteException e3) {
										e3.printStackTrace();
									}

									if (posicoesClicadas.contains(indice)) {
										if (posicoesClicadasAux.contains(indice)) {

											try {
												listaPosicoesUsadas = viraLetrasIterface
														.receberListaPosicaoTabuleiroUsada();
											} catch (RemoteException e2) {
												e2.printStackTrace();
											}

											if (!listaPosicoesUsadas.contains(indice)) {
												letrasDesviradas.append(
														cliente.tabuleiroView.getLabelLetras().get(indice).getText());
												cliente.tabuleiroView.getjTextFieldPalavraFormada()
														.setText(letrasDesviradas.toString());

												try {
													viraLetrasIterface.enviarPosicaoTabuleiroUsada(indice);
												} catch (RemoteException e1) {
													e1.printStackTrace();
												}

											}

										} else {
											posicoesClicadasAux.add(indice);
										}
									} else {
										JOptionPane.showMessageDialog(cliente.tabuleiroView.getjFrameTabuleiro(),
												"Essa peça não foi desvirada");
									}

								}
							}

						});

					}

				}
			});

			cliente.tabuleiroView.getBotaoEnviarPalavraFormada().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					String palavraFormada = cliente.tabuleiroView.getjTextFieldPalavraFormada().getText();
					if (palavraFormada.length() > 0) {
						try {
							viraLetrasIterface.enviarPalavraFormada(cliente.jogador.getId(), palavraFormada);
							cliente.tabuleiroView.getjTextFieldPalavraFormada().setText("");
							cliente.tabuleiroView.getLabelResultadoSorteioDado1().setText("");

							int result = JOptionPane.showConfirmDialog(cliente.tabuleiroView.getjFrameTabuleiro(),
									"Deseja enviar uma nova palavra?", "Confirme", JOptionPane.YES_NO_OPTION);

							if (result == JOptionPane.YES_OPTION) {
								/**
								 * TODO 1. Salvar a lista de indice de palavras
								 * usadas na palavra formada no controler do
								 * tabuleiro 2. Esvaziar a lista de indice de
								 * palavras na implementação de interface
								 */

								cliente.tabuleiroView.getLabelStatusTabuleiro()
										.setText("Você continua na sua vez. Forme mais uma palavra!");
								refatorarEntradaDeDados(cliente, true);
								cliente.tabuleiroView.getBotaoDado1().setEnabled(false);

							} else {
								cliente.tabuleiroView.getLabelStatusTabuleiro()
										.setText("Você passou a vez. Aguarde a jogada do seu adversário.");
								viraLetrasIterface.informarPassarAvez(cliente.jogador.getId());
								refatorarEntradaDeDados(cliente, false);
							}

						} catch (RemoteException e1) {

							e1.printStackTrace();
						}

					} else {
						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você não inseriu uma palavra aqui! D:");
					}
					palavraFormada = "";
				}

			});

			cliente.tabuleiroView.getBotaoCancelar().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						viraLetrasIterface.informarDesistencia(cliente.jogador.getId());
					} catch (RemoteException e1) {
						e1.printStackTrace();
					}

				}
			});

			cliente.tabuleiroView.getBotaoTerminarJogo().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						viraLetrasIterface.informarTerminarJogo(cliente.jogador.getId());
					} catch (RemoteException e1) {
						e1.printStackTrace();
					}

				}
			});

			cliente.tabuleiroView.getBotaoReiniciar().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						viraLetrasIterface.informarReiniciarJogo(cliente.jogador.getId());
					} catch (RemoteException e1) {
						e1.printStackTrace();
					}

				}
			});

			cliente.tabuleiroView.getjFrameTabuleiro().addWindowListener(new java.awt.event.WindowAdapter() {

				@Override
				public void windowClosing(java.awt.event.WindowEvent windowEvent) {
					try {
						viraLetrasIterface.jogadorFechouAJanela(cliente.jogador.getId(), true);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			});

			while (cliente.jogador.isJogando()) {

				if (viraLetrasIterface.checkMensagem(cliente.jogador.getId())) {
					String mensagem = viraLetrasIterface.receberMensagemChat();
					cliente.tabuleiroView.getjTAreaMensagensChat().append(mensagem + "\r\n");
				}

				if (viraLetrasIterface.checkTabuleiro(cliente.jogador.getId())) {
					Tabuleiro tabuleiro = viraLetrasIterface.receberTabuleiro();

					List<Peca> listaPecas = tabuleiro.getListaDePecas();

					int i = 0;
					for (JLabel listaLetras : cliente.tabuleiroView.getLabelLetras()) {
						listaLetras.setText(listaPecas.get(i).getConteudo());
						i++;
					}

					cliente.tabuleiroView.getPainelTabuleiro().setVisible(true);
					cliente.tabuleiroView.getBotaoIniciarJogo().setEnabled(false);
					cliente.tabuleiroView.setTabuleiro(tabuleiro);
				}

				if (viraLetrasIterface.checkJogada(cliente.jogador.getId())
						|| viraLetrasIterface.checkJogadaOutroJogador(cliente.jogador.getId())) {
					Tabuleiro tabuleiro = viraLetrasIterface.receberJogada();
					cliente.tabuleiroView.setTabuleiro(tabuleiro);
					List<Peca> listaPecas = tabuleiro.getListaDePecas();
					int i = 0;
					for (JLabel listaDeLabels : cliente.tabuleiroView.getLabelLetras()) {
						if (!listaPecas.get(i).isUsada()) {
							listaDeLabels.setIcon(null);
						}
						i++;
					}
				}

				if (viraLetrasIterface.checkSorteioDado(cliente.jogador.getId())) {
					int dado = viraLetrasIterface.receberSorteioDado();
					cliente.tabuleiroView.getLabelStatusTabuleiro()
							.setText("Status do Jogo: Seu adversário virará " + (String.valueOf(dado)) + " letras! :D");
				}

				if (viraLetrasIterface.checkPalavraFormada(cliente.jogador.getId())) {
					boolean avaliacao;
					String palavraFormada = viraLetrasIterface.receberPalavraFormada();
					int result = JOptionPane.showConfirmDialog(cliente.tabuleiroView.getjFrameTabuleiro(),
							"Seu adversário enviou " + palavraFormada + ". Ela é válida?", "Jogada do adversário",
							JOptionPane.YES_NO_OPTION);

					if (result == JOptionPane.YES_OPTION) {
						avaliacao = true;
						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você aceitou a palavra enviada!");
					} else {
						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você não aceitou a palavra enviada!");
						avaliacao = false;
					}

					boolean isSalvarPalavraFormada = viraLetrasIterface.checkPalavraAdversario(cliente.jogador.getId(),
							avaliacao);

					if (isSalvarPalavraFormada) {
						viraLetrasIterface.salvarPalavraFormada(cliente.jogador.getId(), palavraFormada);
						cliente.jogador.getListaDePalavrasFormadas().add(palavraFormada);

						refatorarEntradaDeDados(cliente, true);
						viraLetrasIterface.isListar(true);

						List<String> listaPalavrasFormadasJogador = viraLetrasIterface
								.listaPalavrasFormadasJogador(cliente.jogador.getId());

						if (viraLetrasIterface.checkPlayer1(cliente.jogador.getId())) {
							cliente.tabuleiroView.getjTextAreaPalavrasJogador1().setText("");
							StringBuilder listaPalavrasFormadas = new StringBuilder();
							for (String palavrasFormadas : listaPalavrasFormadasJogador) {
								listaPalavrasFormadas.append(" " + palavrasFormadas + "\r\n");
							}
							cliente.tabuleiroView.getjTextAreaPalavrasJogador1()
									.append(listaPalavrasFormadas.toString());

						} else {
							cliente.tabuleiroView.getjTextAreaPalavrasJogador2().setText("");
							StringBuilder listaPalavrasFormadas = new StringBuilder();
							for (String palavrasFormadas : listaPalavrasFormadasJogador) {
								listaPalavrasFormadas.append(" " + palavrasFormadas + "\r\n");
							}
							cliente.tabuleiroView.getjTextAreaPalavrasJogador2()
									.append(listaPalavrasFormadas.toString());
						}

					}

					cliente.tabuleiroView.getPainelTabuleiro().setVisible(true);
					cliente.tabuleiroView.getBotaoIniciarJogo().setEnabled(false);

				}

				if (viraLetrasIterface.checkListaPalavrasFormadasJogador(cliente.jogador.getId())) {
					List<String> listaPalavrasFormadasJogador = viraLetrasIterface
							.listaPalavrasFormadasJogador(cliente.jogador.getId());

					if (viraLetrasIterface.checkPlayer1(cliente.jogador.getId())) {
						cliente.tabuleiroView.getjTextAreaPalavrasJogador1().setText("");
						StringBuilder listaPalavrasFormadas = new StringBuilder();
						for (String palavrasFormadas : listaPalavrasFormadasJogador) {
							listaPalavrasFormadas.append(" " + palavrasFormadas + "\r\n");
						}
						cliente.tabuleiroView.getjTextAreaPalavrasJogador1().append(listaPalavrasFormadas.toString());

					} else {
						cliente.tabuleiroView.getjTextAreaPalavrasJogador2().setText("");
						StringBuilder listaPalavrasFormadas = new StringBuilder();
						for (String palavrasFormadas : listaPalavrasFormadasJogador) {
							listaPalavrasFormadas.append(" " + palavrasFormadas + "\r\n");
						}
						cliente.tabuleiroView.getjTextAreaPalavrasJogador2().append(listaPalavrasFormadas.toString());
					}

				}

				if (viraLetrasIterface.checkTerminarJogo(cliente.jogador.getId())) {

					boolean avaliacao;

					int result = JOptionPane.showConfirmDialog(cliente.tabuleiroView.getjFrameTabuleiro(),
							"Concorda em terminar o jogo?", "Pedido de termino de jogo", JOptionPane.YES_NO_OPTION);

					if (result == JOptionPane.YES_OPTION) {
						avaliacao = true;
					} else {
						avaliacao = false;
					}

					boolean isTerminoAutorizado = viraLetrasIterface.autorizarTermino(cliente.jogador.getId(),
							avaliacao);

					if (isTerminoAutorizado) {
						cliente.jogador.setJogando(false);
						cliente.tabuleiroView.getLabelStatusTabuleiro().setText("Status do Jogo: Jogo terminado!");
						refatorarEntradaDeDados(cliente, false);
						viraLetrasIterface.criarNovoTabuleiroTermino(cliente.jogador.getId());

					}

				} else {

					if (viraLetrasIterface.checkTabuleiroJogoTerminado()) {
						cliente.tabuleiroView.getLabelStatusTabuleiro().setText("Status do Jogo: Você terminou o jogo");
						refatorarEntradaDeDados(cliente, false);
					}

				}

				if (viraLetrasIterface.checkDesistencia(cliente.jogador.getId())) {

					boolean avaliacao;

					int result = JOptionPane.showConfirmDialog(cliente.tabuleiroView.getjFrameTabuleiro(),
							"Permite que seu adversário desista?", "Pedido de desistência", JOptionPane.YES_NO_OPTION);

					if (result == JOptionPane.YES_OPTION) {
						avaliacao = true;
						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você aceitou a desistência!");

					} else {
						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você não aceitou a desistência!");
						avaliacao = false;

					}

					boolean isDesistenciaAutorizada = viraLetrasIterface.autorizarDesistencia(cliente.jogador.getId(),
							avaliacao);

					if (isDesistenciaAutorizada) {
						cliente.jogador.setVencedor(true);
						cliente.jogador.setJogando(false);
						refatorarEntradaDeDados(cliente, false);

						viraLetrasIterface.criarNovoTabuleiroDesistente(cliente.jogador.getId());

					}
				} else {

					if (viraLetrasIterface.checkTabuleiroCancelado(cliente.jogador.getId())) {

						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você desistiu do jogo");
						refatorarEntradaDeDados(cliente, false);
					}
				}

				if (viraLetrasIterface.checkReiniciarJogo(cliente.jogador.getId())) {
					boolean avaliacao;

					int result = JOptionPane.showConfirmDialog(cliente.tabuleiroView.getjFrameTabuleiro(),
							"Permite que seu adversário reinicie o jogo?", "Pedido de reinicio",
							JOptionPane.YES_NO_OPTION);

					if (result == JOptionPane.YES_OPTION) {
						avaliacao = true;
						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você aceitou o reinício da partida!");

					} else {
						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você não aceitou o reinício da partida!");
						avaliacao = false;

					}

					boolean isDesistenciaAutorizada = viraLetrasIterface.autorizarReinicio(cliente.jogador.getId(),
							avaliacao);

					if (isDesistenciaAutorizada) {
						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você reiniciou a partida!");
						viraLetrasIterface.criarNovoTabuleiroRenicio(cliente.jogador.getId());
						Tabuleiro novoTabuleiro = viraLetrasIterface.tabuleiroReiniciado();
						viraLetrasIterface.enviarTabuleiro(cliente.jogador.getId(), novoTabuleiro);
						cliente.tabuleiroView.setTabuleiro(novoTabuleiro);

					}
				} else {

					if (viraLetrasIterface.checkTabuleiroJogoTerminado()) {

						cliente.tabuleiroView.getLabelStatusTabuleiro()
								.setText("Status do Jogo: Você reiniciou o jogo!");
						refatorarEntradaDeDados(cliente, false);
					}
				}

				if (viraLetrasIterface.checkJanelaAberta(cliente.jogador.getId())) {
					String mensagem = "Status do Jogo: Seu adversário fechou a janela.";
					cliente.tabuleiroView.getLabelStatusTabuleiro().setText(mensagem);
					viraLetrasIterface.mudarStatusJanela();

				}

			}

		} catch (

		RemoteException ex) {
			Logger.getLogger(ClienteController.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	public static void refatorarEntradaDeDados(final ClienteController cliente, boolean estado) {
		cliente.tabuleiroView.getBotaoDado1().setEnabled(estado);
		cliente.tabuleiroView.getBotaoCancelar().setEnabled(estado);
		cliente.tabuleiroView.getBotaoReiniciar().setEnabled(estado);
		cliente.tabuleiroView.getjTextFieldPalavraFormada().setEnabled(estado);
		cliente.tabuleiroView.getBotaoEnviarPalavraFormada().setEnabled(estado);
		cliente.tabuleiroView.getjTextFieldMensagemChat().setEnabled(estado);
	}

	public Jogador getJogador() {
		return jogador;
	}

	public void setJogador(Jogador jogador) {
		this.jogador = jogador;
	}

	public JTextField getNomeJogador() {
		return nomeJogador;
	}

	public void setNomeJogador(JTextField nomeJogador) {
		this.nomeJogador = nomeJogador;
	}

	public TabuleiroView getTabuleiroView() {
		return tabuleiroView;
	}

	public void setTabuleiroView(TabuleiroView tabuleiroView) {
		this.tabuleiroView = tabuleiroView;
	}

	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}

	public void setTabuleiro(Tabuleiro tabuleiro) {
		this.tabuleiro = tabuleiro;
	}

}
