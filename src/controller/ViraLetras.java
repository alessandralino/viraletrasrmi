package controller;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Jogador;
import model.Peca;
import model.Tabuleiro;

public class ViraLetras extends UnicastRemoteObject implements ViraLetrasInterface {
	Map<Integer, Jogador> jogadores;
	List<Integer> listaDePosicoesDesviradas;
	List<Integer> listaDePosicoesUsadas;

	int idUltimoChat;
	int idUltimoTabuleiro;
	int idUltimaJogada;
	int idJogadorFechouAJanela;
	int idJogadorDesistiu;
	int idJogadorPassouAvez;
	int idJogadorTerminouJogo;
	int idPlayer1;
	int idJogadorReiniciarJogo;

	String mensagemChat;
	String palavraFormada;

	boolean isMensagem;
	boolean isTabuleiro;
	boolean isJogada;
	boolean isSorteio;
	boolean isPalavraValida;
	boolean isPalavraFormada;
	boolean isListado;
	boolean isJanelaFechada;
	boolean isSolicitaDesistencia;
	boolean isVitoria;
	boolean isJogoCancelado;
	boolean isLetrasNaPalavraFormada;
	boolean isSolicitaTerminarJogo;
	boolean isJogoTerminado;
	boolean isPassarAvezOutroJogador;
	boolean isSolicitaReiniciarJogo;
	boolean isJogoReniciado;
	boolean isVezJogar;

	Tabuleiro tabuleiroJogo;

	public ViraLetras() throws RemoteException {
		super();
		this.jogadores = new HashMap();
		this.listaDePosicoesDesviradas = new ArrayList<Integer>();
		this.listaDePosicoesUsadas = new ArrayList<Integer>();
		this.isJanelaFechada = false;
		this.isVitoria = false;
		this.isSolicitaDesistencia = false;
		this.isSolicitaTerminarJogo = false;
		this.isJogoCancelado = false;
		this.isJogoTerminado = false;
		this.isSolicitaReiniciarJogo = false;
		this.isVezJogar = false;
		this.isPassarAvezOutroJogador = false;

	}

	@Override
	public int conectarCliente(String nome, Tabuleiro tabuleiro) throws RemoteException {
		int idJogador = jogadores.size();

		Jogador novoJogador = new Jogador();
		jogadores.put(idJogador, novoJogador);
		jogadores.get(idJogador).setId(idJogador);
		jogadores.get(idJogador).setNome(nome);

		System.out
				.println(jogadores.get(idJogador).getId() + ":" + jogadores.get(idJogador).getNome() + " se conectou.");

		return idJogador;
	}

	@Override
	public void enviarMensagemChat(int id, String mensagem) throws RemoteException {
		isMensagem = true;
		idUltimoChat = id;
		mensagemChat = mensagem;

	}

	@Override
	public String receberMensagemChat() throws RemoteException {
		isMensagem = false;
		String nome = jogadores.get(idUltimoChat).getNome();
		return nome + ": " + mensagemChat;
	}

	@Override
	public boolean checkMensagem(int id) throws RemoteException {
		return !(idUltimoChat == id || isMensagem == false);
	}

	@Override
	public void enviarTabuleiro(int id, Tabuleiro tabuleiro) throws RemoteException {
		idPlayer1 = id;
		isTabuleiro = true;
		idUltimoTabuleiro = id;
		tabuleiroJogo = tabuleiro;
	}

	@Override
	public boolean existemAdversarios() throws RemoteException {
		return jogadores.size() > 1;
	}

	@Override
	public boolean checkTabuleiro(int id) throws RemoteException {
		return !(idUltimoTabuleiro == id || isTabuleiro == false);
	}

	@Override
	public Tabuleiro receberTabuleiro() throws RemoteException {
		isTabuleiro = false;
		return tabuleiroJogo;
	}

	@Override
	public void enviarPosicaoTabuleiroDesvirada(int posicao) throws RemoteException {
		isJogada = true;
		tabuleiroJogo.getListaDePecas().get(posicao).setUsada(false);
		listaDePosicoesDesviradas.add(posicao);
	}
	
	public void enviarPosicaoTabuleiroUsada(int posicao) throws RemoteException {
		tabuleiroJogo.getListaDePecas().get(posicao).setTabuleiro(false);
		tabuleiroJogo.getListaDePecas().get(posicao).setConteudo("");
		listaDePosicoesUsadas.add(posicao);
	}
	
	
	public List<Integer> receberListaPosicaoTabuleiroUsada() throws RemoteException {
		return listaDePosicoesUsadas;
	}
	
	public Tabuleiro receberTabuleiroSemLetrasUsadas() throws RemoteException {
		return tabuleiroJogo;
	}
	

	@Override
	public boolean checkJogada(int id) throws RemoteException {
		return !(idUltimoTabuleiro == id || isJogada == false);
	}
	
	@Override
	public boolean checkJogadaOutroJogador(int id) throws RemoteException {
		return !(idUltimoTabuleiro != id || isJogada == false);
	}

	@Override
	public Tabuleiro receberJogada() throws RemoteException {
		isJogada = false;
		return tabuleiroJogo;
	}

	@Override
	public void enviarSorteioDado(int sorteio) throws RemoteException {
		isSorteio = true;
		tabuleiroJogo.setDado(sorteio);

	}

	@Override
	public int receberSorteioDado() throws RemoteException {
		isSorteio = false;
		return tabuleiroJogo.getDado();

	}

	@Override
	public boolean checkSorteioDado(int id) throws RemoteException {
		return !(idUltimoTabuleiro == id || isSorteio == false);
	}

	@Override
	public void enviarPalavraFormada(int id, String palavra) throws RemoteException {
		isPalavraFormada = true;
		idUltimaJogada = id;
		palavraFormada = palavra;
	}

	@Override
	public boolean checkPalavraFormada(int id) throws RemoteException {
		return !(idUltimaJogada == id || isPalavraFormada == false);
	}

	@Override
	public boolean checkPalavraAdversario(int id, boolean avaliacao) throws RemoteException {
		idUltimaJogada = id;
		return isPalavraValida = avaliacao;
	}

	@Override
	public String receberPalavraFormada() throws RemoteException {
		isPalavraFormada = false;
		return palavraFormada;

	}

	@Override
	public void salvarPalavraFormada(int id, String palavraFormada) throws RemoteException {
		isListado = false;
		jogadores.get(id).getJogada().setPalavraFormada(palavraFormada);
		jogadores.get(id).getListaDePalavrasFormadas().add(palavraFormada);
	}

	@Override
	public List<String> listaPalavrasFormadasJogador(int id) throws RemoteException {
		isListado = true;
		return jogadores.get(id).getListaDePalavrasFormadas();

	}

	@Override
	public boolean checkListaPalavrasFormadasJogador(int id) throws RemoteException {
		return (jogadores.get(id).getListaDePalavrasFormadas().size() > 0 && isListado == false);
	}

	@Override
	public void isListar(boolean listar) throws RemoteException {
		isListado = false;

	}

	@Override
	public String nomeJogador(int id) throws RemoteException {
		return jogadores.get(id).getNome();
	}

	public Map<Integer, Jogador> getJogadores() throws RemoteException {
		return jogadores;
	}

	@Override
	public void jogadorFechouAJanela(int id, boolean janelaStatus) throws RemoteException {
		idJogadorFechouAJanela = id;
		isJanelaFechada = janelaStatus;
	}

	@Override
	public boolean checkJanelaAberta(int id) throws RemoteException {
		return !(idJogadorFechouAJanela == id || isJanelaFechada == false);
	}

	@Override
	public void mudarStatusJanela() throws RemoteException {
		isJanelaFechada = false;

	}

	@Override
	public void informarDesistencia(int id) throws RemoteException {
		idJogadorDesistiu = id;
		isSolicitaDesistencia = true;
	}

	@Override
	public void informarPassarAvez(int id) throws RemoteException {
		idJogadorPassouAvez = id;
		isPassarAvezOutroJogador = true;
	}

	public boolean checkPassarAvez(int id) throws RemoteException {
		return (idJogadorPassouAvez != id && isPassarAvezOutroJogador == true);
	}

	@Override
	public void mudarStatusDesistencia(boolean avaliacao) throws RemoteException {
		isSolicitaDesistencia = avaliacao;

	}

	@Override
	public boolean checkDesistencia(int id) throws RemoteException {
		return !(idJogadorDesistiu == id || isSolicitaDesistencia == false);
	}

	@Override
	public boolean autorizarDesistencia(int id, boolean avaliacao) throws RemoteException {
		isSolicitaDesistencia = avaliacao;
		return isSolicitaDesistencia;
	}

	public boolean autorizarReinicio(int id, boolean avaliacao) throws RemoteException {
		isSolicitaReiniciarJogo = avaliacao;
		return isSolicitaReiniciarJogo;
	}

	@Override
	public void informarTerminarJogo(int id) throws RemoteException {
		idJogadorTerminouJogo = id;
		isSolicitaTerminarJogo = true;
	}

	@Override
	public void informarReiniciarJogo(int id) throws RemoteException {
		idJogadorReiniciarJogo = id;
		isSolicitaReiniciarJogo = true;
	}

	@Override
	public boolean autorizarTermino(int id, boolean avaliacao) throws RemoteException {
		isSolicitaTerminarJogo = avaliacao;
		return isSolicitaTerminarJogo;
	}

	@Override
	public boolean checkTerminarJogo(int id) throws RemoteException {
		return !(idJogadorTerminouJogo == id || isSolicitaTerminarJogo == false);
	}

	public boolean checkReiniciarJogo(int id) throws RemoteException {
		return !(idJogadorReiniciarJogo == id || isSolicitaReiniciarJogo == false);

	}

	@Override
	public void criarNovoTabuleiroDesistente(int id) throws RemoteException {
		isJogoCancelado = true;
		jogadores.get(id).setJogando(false);
		jogadores.get(id).setDesistente(true);

	}

	@Override
	public void criarNovoTabuleiroRenicio(int id) throws RemoteException {
		isJogoReniciado = true;
		tabuleiroJogo = new Tabuleiro();

	}

	@Override
	public void criarNovoTabuleiroTermino(int id) throws RemoteException {
		isJogoTerminado = true;
		jogadores.get(id).setJogando(false);

	}

	@Override
	public Tabuleiro tabuleiroCancelado() throws RemoteException {
		return tabuleiroJogo;
	}

	@Override
	public Tabuleiro tabuleiroReiniciado() throws RemoteException {
		isSolicitaReiniciarJogo = false;
		return tabuleiroJogo;
	}

	@Override
	public boolean checkTabuleiroCancelado(int id) throws RemoteException {
		return isJogoCancelado;
	}

	@Override
	public boolean checkTabuleiroJogoTerminado() throws RemoteException {
		return isJogoTerminado;
	}

	@Override
	public List<Peca> getListaDePosicoesUsadasNaPalavra(int id) throws RemoteException {
		return jogadores.get(idUltimaJogada).getJogada().getListaPecasCompoemPalavraFormada();
	}

	@Override
	public List<Integer> getListaPosicoesDesviradas() throws RemoteException {
		return listaDePosicoesDesviradas;
	}

	@Override
	public boolean checkPlayer1(int id) throws RemoteException {
		return !(id == idPlayer1);
	}

}
